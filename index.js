var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
// Our game classes.
var Server = require('./game/server');

// Create singleton server.
var server = new Server(io);

// Give main client page.
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
// Give game logic files.
app.get('/game/:file', function(req, res) {
    res.sendFile(__dirname + '/game/' + req.params.file);
});
// Give assets.
app.get('/assets/:file', function(req, res) {
    res.sendFile(__dirname + '/assets/' + req.params.file);
});

io.on('connection', function(socket){
    // Handles the basic chat client.
    io.emit('chat message', 'A new user connected.');
    socket.on('disconnect', function() {
        console.log('a user disconnected.');
    });
    socket.on('chat message', function(msg) {
        console.log('Received message: ' + msg);
        io.emit('chat message', msg);
        //io.emit('chat message', bot.getResponse());
    });
    
    // Handle client bindings
    server.onConnect(socket);
    // Bindings
    socket.on('disconnect', function() {
        server.onDisconnect(socket);
    });
    socket.on('other create', function(other) {
        server.onClientCreate(socket, other);
    });
    // Movement
    socket.on('pusheen move', function(x, y) {
        server.onMove(socket, x, y);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
