/**
 * Creates a version of an other object from a client object.
 * Will also create a version for other objects.
 * This allows for better communication using sockets.
 */
var Other = function(client) {
    this.type = "other";
    this.pusheen = new Pusheen(client.pusheen.x, client.pusheen.y);
}
Other.prototype.draw = function() {
    this.pusheen.draw();
}
