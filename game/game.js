/**
 * This variable contains most of the information about the game
 * including the attributes about how the game functions.
 * This is an 'object' in javascript. Think of it like a mini-class
 * in another language, except it is tied to a variable.
 */
var game = {
	width: 640,
	height: 480,
	fps: 60,
	fps_tick_ratio: this.fps / 30,
	tick_second_ratio: 60 / 1000,
	height_ratio: 1,
	width_ratio: 1,
    
	init: function() {
        console.log("Game started!");
		clock.start(); // inits tick and basic game functions
	}
}
// clock object, keeps track of gametime and runs render/update
clock = {
    tick: 0,
    increment: 1 / game.fps_tick_ratio,

    start: function () {
        this.interval = setInterval(function() {
            clock.tick += clock.increment;
            update();
            render();
        }, 1000/game.fps);
    },
    ispaused: function() {
        if (this.interval == null) {
            return true;
        }
        else {
            return false;
        }
    },
    reset: function(clockedin) {
        if (clockedin == true) {
            this.tick = 0;
        }
        clearInterval(this.interval);
        delete this.interval;
        this.start();
    },
    pause: function () {
        console.log("Pausing game!");
        clearInterval(this.interval);
        delete this.interval;
    },

    resume: function () {
        if (!this.interval) this.start();
    }
};