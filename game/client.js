// Let's make a background for our game.
var background = new Image();
// This tells us where the background is!
background.src = "assets/background.jpg";

var Client = function(socket) {
    // Create ourself and announce it.
    this.socket = socket;
    this.init(socket);
    // Set up bindings to self into socket.
    this.bind();
}
// Sets up base client objects.
Client.prototype.init = function(socket) {
    // Create us
    this.pusheen = new Pusheen(Math.random() * 200, 40);
    // create others.
    this.others = {};
    // Announces creation.
    console.log("Self Creation.");
    this.socket.emit('other create', new Other(this));
}
// Called when another player joins.
Client.prototype.join = function(id, other) {
    console.log("Other Joining: " + id);
    this.others[id] = new Other(other);
}
// Called when a player leaves.
Client.prototype.leave = function(id) {
    console.log("Other Leaving: " + id);
    delete this.others[id];
}
Client.prototype.otherMove = function(id, x, y) {
    this.others[id].pusheen.setPos(x, y);
}
/**
 * Attaches socket calls to server.
 * Expects pusheen exists.
 * Binds to set socket object.
 */
Client.prototype.bind = function() {
    // Bind joining for other players.
    this.socket.on('other join', this.join.bind(this));
    this.socket.on('other leave', this.leave.bind(this));
    // Movement
    this.socket.on('pusheen move', this.otherMove.bind(this));
}
/** Draws the client game */
Client.prototype.draw = function() {
    // Draw others.
    for (var id in this.others) {
        this.others[id].draw();
    }
    // Draw us on top.
    this.pusheen.draw();
}
/** Updates the client object */
Client.prototype.update = function() {
    // Check key bindings
    if ("w" in keysDown || "ArrowUp" in keysDown) { // Player holding up
		this.pusheen.moveUp(5);
        this.socket.emit('pusheen move', this.pusheen.x, this.pusheen.y);
	}
	if ("s" in keysDown || "ArrowDown" in keysDown) { // Player holding down
		this.pusheen.moveDown(5);
        this.socket.emit('pusheen move', this.pusheen.x, this.pusheen.y);
	}
	if ("a" in keysDown || "ArrowLeft" in keysDown) { // Player holding left
		this.pusheen.moveLeft(5);
        this.socket.emit('pusheen move', this.pusheen.x, this.pusheen.y);
	}
	if ("d" in keysDown || "ArrowRight" in keysDown) { // Player holding right
		this.pusheen.moveRight(5);
        this.socket.emit('pusheen move', this.pusheen.x, this.pusheen.y);
	}
}
// Create our client with socket from IO.
var client = new Client(socket);

// Runs every about 16 ms depending on updates per second
// This, similar to LUA, is how pusheen is moved when we press keys.
// The number is the ascii character!
function update() {
    client.update();
}

/**
 * This function is called right after update is called.
 * It clears the screen, draws the background, then then
 * draws pusheen.
 * The reason why the screen is cleared is so that any changes are
 * shown. If you turn it off, you'll see what happens!
 */
function render() {
	ctx.save();
	// Clears the screen with white (comment out to see what happens part1)
    ctx.clearRect(0, 0, game.width, game.height);
	// Draws the background at the origin! (comment out to see what happens part2)
	ctx.drawImage(background, 0, 0);

    // Draws the client.
    client.draw();

	ctx.restore();
}
