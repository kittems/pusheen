/**
 * Contains the necessary information to update the game information about
 * a single client: The 'other' client object and the socket associated.
 */
var GameClient = function(socket) {
    this.socket = socket;
}
// Attaches the other object to this game client object.
GameClient.prototype.attachOther = function(other) {
    this.me = other;
}

/**
 * Creates a server with an empty set of clients.
 */
var Server = function(io) {
    console.log("I am created.");
    // This is not a client object for the game, but for sockets.
    this.clients = {}
    this.io = io;
};
/** Gets client object associated with socket */
Server.prototype.getClient = function(socket) {
    return this.clients[socket.id];
}
/**
 * Called when a user connects.
 */
Server.prototype.onConnect = function(socket) {
    console.log("A new player has joined: " + socket.id);
    // Add to clients array.
    this.clients[socket.id] = new GameClient(socket);
};
/** Called when user initializes. */
Server.prototype.onClientCreate = function(socket, other) {
    console.log("New player has initialized: " + socket.id);
    this.getClient(socket).attachOther(other);
    // Join socket to room 'pusheens'
    socket.join('pusheens');
    // Broadcast to others that I exist!
    socket.broadcast.to('pusheens').emit('other join', socket.id, other);
    // Everyone should broadcast their self to me.
    for (var key in this.clients) {
        var id = this.clients[key].socket.id;
        // Except the client who is me.
        if (id != socket.id) {
            this.io.to(socket.id).emit('other join', id, this.clients[key].me);
        }
    }
};
Server.prototype.onClientDestroy = function(socket) {
    console.log("A player destroyed: " + socket.id);
    socket.broadcast.to('pusheens').emit('other leave', socket.id);
};
// Called when a pusheen moves.
Server.prototype.onMove = function(socket, newX, newY) {
    // Update game objects.
    this.getClient(socket).me.pusheen.x = newX;
    this.getClient(socket).me.pusheen.y = newY;
    // Tell others
    socket.broadcast.to('pusheens').emit('pusheen move', socket.id, newX, newY);
};
/**
 * Called when a user disconnects.
 */
Server.prototype.onDisconnect = function(socket) {
    console.log("A player has left: " + socket.id);
    // Tell others
    this.onClientDestroy(socket);
    // Remove from clients array.
    delete this.clients[socket.id];
};

// Add to nodeJs
module.exports = Server;
