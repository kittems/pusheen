/**
 * The main pusheen class.
 * @param x The starting x position
 * @param y the starting y position.
 */
var Pusheen = function(x, y) {
    this.image = new Image();
    this.image.src = "assets/pusheen.png";
    this.x = x;
    this.y = y;
};
Pusheen.prototype.moveUp = function(amount) {
    this.y -= amount;
}
Pusheen.prototype.moveDown = function(amount) {
    this.y += amount;
}
Pusheen.prototype.moveLeft = function(amount) {
    this.x -= amount;
}
Pusheen.prototype.moveRight = function(amount) {
    this.x += amount;
}
Pusheen.prototype.setPos = function(x, y) {
    this.x = x;
    this.y = y;
}
Pusheen.prototype.draw = function() {
    ctx.drawImage(this.image, this.x, this.y);
};
